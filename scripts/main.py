'''
Main script. Run the file and insert your account details 
The code will search the newest free coupons and check if they are valid
'''

import coupon_search
from reportlab.pdfgen import canvas
from reportlab.lib import pagesizes

output_file = 'coupons.pdf'

import time
t1 = time.time()
print("Execution started")
print("Collecting discounts")
discounts = coupon_search.get_discounts()
print(len(discounts)," discounts found")
print('time passed',time.time()-t1)

pdf = canvas.Canvas(output_file,pagesize=pagesizes.A4)
fontsize = 12
fontname = 'Times-Roman'
pdf.setFont(fontname,fontsize)
top = pagesizes.A4[1]
pos,left = 0,0
items_per_page = 30
base = top
step = pagesizes.A4[1]//items_per_page
i = 0
discount_i = 1
for url,name in discounts.items():
    name = str(discount_i) + ' - ' + name
    pdf.drawString(left, base - fontsize, name)
    pdf.linkURL(url, (pdf.stringWidth(name), base - fontsize/4, left, base - fontsize))
    
    base -= step
    i+=1
    if i == items_per_page:
        i = 0
        base = top
        pdf.showPage()
    discount_i+=1
pdf.save()