# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 14:23:00 2017

@author: icaromarley5

Wrap-up for selenium functions
"""

from selenium import webdriver
import time
import threads
from selenium.webdriver.chrome.options import Options  

max_count = 10
debug = False 
chrome_path = '../drivers/chromedriver.exe' #chromedriver
count = 0  
finalized = False  
watch_dog = None
check_interval = 1 * 60 * 0.5 # 30 seconds

def create_browser(limit=5):  # 5 h
    global count
    count += 1
    chrome_options = Options()  
    if not debug:
        chrome_options.add_argument("--headless") 
    driver = webdriver.Chrome(chrome_path,chrome_options=chrome_options)
    driver.start_time = time.time()
    driver.time_limit = limit
    driver.is_active = False
    return driver
browser_list = []
def create_browsers():
    global browser_list
    browser_list = [create_browser() for i in range(max_count)]

def destroy_browsers():
    global browser_list
    [browser.close() for browser in browser_list]

# creates processing threads based on max number of active browsers
# task inputs are a list and a browser
# task returns a list
allocated = False # flags if process_task allocated browsers
def process_task(task,input_list=None):
    global allocated
        
    free_browser_list =  [browser for browser in browser_list \
                              if not browser.is_active] # all free browsers
    if input_list:
        n_input = len(input_list)
        thread_list = []
        n_browser = len(free_browser_list)
        step = n_input // n_browser
            
        if step == 0:
            step = 1
        # split output and throw task threads for each split
        base = 0
        for index, browser in enumerate(free_browser_list):
            max_limit = False
            browser.is_active = True
            floor = base
            top = base + step
            if (top + step >= n_input):  # last browser
                top += step  # get whats left of the input
                max_limit = True
            args = {"input_list": input_list[floor: top], "browser": browser}
            thread = threads.TaskThread(task, args)
            # starts thread
            thread.start()
            thread_list.append(thread)
            base += step
            if max_limit:
                break
        # finished allocating browsers
        allocated = True
    
        results = []
        for t in thread_list:
            results += t.join()
        
        # release all used browsers
        for browser in free_browser_list:
            browser.is_active = False
    else:
        browser = free_browser_list[0]
        args = {"browser": browser}
        if input_list:
            args['input_list'] = input_list
        thread = threads.TaskThread(task, args)
        # starts thread
        thread.start()
        # finished allocating browsers
        browser.is_active = True
        allocated = True  
        results = thread.join()
        
        # release all used browsers
        browser.is_active = False
    return results

def process_multi_tasks(functions,input_list=None):
    global allocated
    thread_list = []
    args = {}
    if input_list:
        args['input_list'] = input_list
    
    create_browsers()
    for function in functions:  
        allocated = False
        args["task"] = function
        thread = threads.TaskThread(process_task, args)
        #starts thread
        thread.start()
        thread_list.append(thread)
        
        while not(allocated and [browser for browser in browser_list \
                                  if not browser.is_active]):
            continue
    
    # collect results
    results = []
    for t in thread_list:
        results+=t.join()
        
    destroy_browsers()
    return results