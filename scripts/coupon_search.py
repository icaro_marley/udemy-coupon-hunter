'''

Functions related to coupons

'''
import browsers
import re
import sites
import inspect
     
# get discounts 
def get_discounts():
    functions = inspect.getmembers(sites, inspect.isfunction)
    functions = [t[1] for t in functions if inspect.getmodule(t[1]) == sites]
    coupons_list = browsers.process_multi_tasks(functions) 
    print("{} coupons found".format(len(coupons_list)))
    # checks valid urls
    print("Checking urls")
    # filter duplicates and in the wrong format
    # check if coupons are from udemy website
    coupons_list = browsers.process_multi_tasks([check_discounts],coupons_list)
    coupons_dict = {}
    for coupon in coupons_list:
        coupons_dict.update(coupon)
    print("{} coupons remain after processing".format(len(coupons_dict)))
    return coupons_dict

# remove duplicates, without coupons and non udemy urls
def check_discounts(browser,input_list):
    coupons = []
    for coupon_dict in input_list:
        url = list(coupon_dict.keys())[0]
        name = coupon_dict[url]
        browser.get(url)
        if "udemy.com" in browser.current_url:
            pattern = r'https:\/\/www\.udemy\.com\/[^\/.]*\/'
            free = len(re.sub(pattern,'',url)) == 0
            if (not free):
                coupons.append({url:name})
    return coupons