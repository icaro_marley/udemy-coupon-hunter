'''

Site specific crawling functions

'''
import inspect
from warning_errors import write_warning
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

def get_all_discountsglobal_links(browser):
    url = "https://coursespoints.com/"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in browser.find_elements_by_xpath('//h4[@class="pt-cv-title"]/a')]
                    
        for link in links:
            browser.get(link)
            name = browser.find_element_by_xpath('//h1[@class="container"]').text
            try:
                url = browser.find_element_by_xpath('//div[@class="link-holder"|@class="entry-content description"]/p/a[contains(@href,"www.udemy.com/")]').get_attribute('href')
            except:
                continue
            coupons.append({url:name})
    except Exception as e:
        print(e) 
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons 

def get_all_bestcouponhunter_links(browser):
    url = "https://bestcouponhunter.com/"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in browser.find_elements_by_xpath('//h3/a')]
                    
        for link in links:
            browser.get(link)
            name = browser.find_element_by_xpath('//h1').text
            url = browser.find_element_by_xpath('//a[contains(text(),"GET COUPON")]').get_attribute('href')
            coupons.append({url:name})
    except Exception as e:
        print(e)        
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons
    
def get_all_discudemy_links(browser):
    url = "https://www.discudemy.com/all"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in  browser.find_elements_by_xpath('//a[@class="card-header"]')]
                    
        for link in links:
            browser.get(link)
            name = browser.find_element_by_xpath('//h1[contains(@class,"ui header")]').text
            url = browser.find_element_by_xpath('//a[contains(text(),"Take Course")]').get_attribute('href')
            browser.get(url)
            url = browser.find_element_by_xpath('//div[@class="ui segment"]/a').get_attribute('href')
            coupons.append({url:name})
    except Exception as e:
        print(e)        
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons 
    
def get_all_smartybro_links(browser):
    url = "https://smartybro.com/category/udemy-coupon-100-off/"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in  browser.find_elements_by_xpath('//h2[@class="grid-tit"]/a')]
                    
        for link in links:
            browser.get(link)
            name = browser.find_element_by_xpath('//a[contains(@class,"fasc-button")]').text
            url = browser.find_element_by_xpath('//a[contains(@class,"fasc-button")]').get_attribute('href')
            coupons.append({url:name})
    except Exception as e:
        print(e)          
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons 
    
def get_all_udemydiscountcoupon_links(browser):
    url = "https://udemydiscountcoupon.com/category/udemy-coupon-100-off/"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in  browser.find_elements_by_xpath('//a[@class="more-link button"]')]
                    
        for link in links:
            browser.get(link)
            name = browser.find_element_by_xpath('//h1[@class="post-title entry-title"]').text
            url = browser.find_element_by_xpath('//a[contains(@href,"udemy.com/")]').get_attribute('href')
            coupons.append({url:name})
    except Exception as e:
        print(e)          
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons 
    
def get_all_couponscorpion_links(browser):
    url = "https://couponscorpion.com/"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in  browser.find_elements_by_xpath('//h3/a')]
                    
        for link in links:
            browser.get(link)
            name = browser.find_element_by_xpath('//h1').text
            try:
                wait = WebDriverWait(browser, 10)
                url = wait.until(ec.visibility_of_element_located((By.XPATH, '//a[contains(text(),"GET COUPON CODE")]'))).get_attribute('href')
            except:
                continue
            coupons.append({url:name})
    except Exception as e:
        print(e)          
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons 
    
def get_all_realdiscount_links(browser):
    url = "https://www.real.discount/new/page/1"
    coupons = []
    try:
        browser.get(url)
    
        links = [a.get_attribute('href') for a in  browser.find_elements_by_xpath('//div[contains(@class,"white-block-content")]/h4/a')]
                    
        for link in links:
            browser.get(link)
            url = browser.find_element_by_xpath('//a[contains(text(),"Get Coupon")]').get_attribute('href')
            coupons.append({url:browser.title})
    except Exception as e:
        print(e)          
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons 

def get_all_moocera_links(browser):
    url = "https://moocera.org/"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in browser.find_elements_by_xpath('//h2[@class="entry-title post-title"]/a')]
        
        for link in links:
            browser.get(link)
            url = browser.find_element_by_xpath('//a[contains(@href,"udemy.com/")]').get_attribute('href')
            coupons.append({url:browser.title}) 
    except Exception as e:
        print(e)        
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons 


def get_all_promocoupons24_links(browser):
    url = "http://www.promocoupons24.com/"
    coupons = []
    try:
        browser.get(url)
        browser.find_element_by_id("popupContactClose").click()
        links = [a.get_attribute('href') for a in browser.find_elements_by_xpath('//div[@class="post-body entry-content"]/div/a')]
    
        for link in links:
            browser.get(link)
            try:
                url = browser.find_element_by_xpath('//a[contains(@href,"www.udemy.com/")]').get_attribute('href')
            except:
                continue
            coupons.append({url:browser.title}) 
    except Exception as e:
        print(e)            
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')    
    return coupons 


def get_all_scrollcoupons_links(browser,input_list = [1]):
    url = "https://www.scrollcoupons.com/"
    coupons = []
    try:
        browser.get(url)    
        links = [a.get_attribute('href') for a in browser.find_elements_by_xpath('//h3[@class="item-title"]/a')]
        
        for link in links:
            browser.get(link)
            url = browser.find_element_by_xpath('//a[contains(text(),"www.udemy.com/")]').get_attribute('href')
            coupons.append({url:browser.title}) 
    except Exception as e:
        print(e)       
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  

    return coupons    

def get_all_anycouponcode_links(browser):
    url = "http://www.anycouponcode.net/tag/100-off/?sort=newest"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in browser.find_elements_by_xpath('//h2[@class="alt"]/a')]
        for link in links:
            browser.get(link)
            url = browser.find_element_by_xpath('//a[contains(text(),"www.udemy.com/")]').get_attribute('href')
            coupons.append({url:browser.title}) 
    except Exception as e:
        print(e)             
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  

    return coupons    

def get_all_udemycoupon_links(browser,input_list=[1]):
    url = "http://udemycoupon.discountsglobal.com/coupon-category/free-2/"
    coupons = []
    
    try:
        browser.get(url)                
        for div in browser.find_elements_by_xpath('//div[@class="item-holder"]/div[@class="item-frame row"]'):
            name = div.find_element_by_xpath('.//h3[@class="entry-title"]/a').text
            url = div.find_element_by_xpath('.//div[@class="link-holder"]/a').get_attribute('href')
            coupons.append({url:name})
    except Exception as e:
        print(e)           
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons  

def get_all_onlinecoursesupdate_links(browser):
    url = "http://www.onlinecoursesupdate.com/"
    coupons = []
    try:
        browser.get(url)
        links = [a.get_attribute('href') for a in browser.find_elements_by_xpath('//h3[@class="post-title entry-title"]/a')]
    
        for link in links:
            browser.get(link)
            name = browser.find_element_by_xpath('//h3[@class="post-title entry-title"]').text
            url = browser.find_element_by_xpath('//a[contains(text(),"Take This Course")]').get_attribute('href')
            coupons.append({url:name}) 
    except Exception as e:
        print(e)    
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')  
    return coupons

def get_all_learnviral_links(browser,input_list=[1]):
    url = "https://udemycoupon.learnviral.com/coupon-category/free100-discount/"
    coupons = []
    try:
        browser.get(url)
        for div in browser.find_elements_by_xpath('//div[@class="item-holder"]'):
            name = div.find_element_by_xpath('.//h3[@class="entry-title"]/a').text
            url = div.find_element_by_xpath('.//div[@class="link-holder"]/a').get_attribute('href')
            coupons.append({url:name})
    except Exception as e:
        print(e)    
    if coupons == []:
        func_name = inspect.stack()[0][3]
        write_warning(func_name+' \n')       
    return coupons