Versão mais pesada da aplicação que automatiza a tarefa de encontrar e coletar cupons de desconto da plataforma de ensino Udemy.
Os cupons são coletados em sites específicos e armazenados em arquivo.

Por usar um navegador pesado, o projeto não é recomendado para ser executado dentro de servidores.

Atualmente, o projeto passa por dificuldades envolvendo o CAPTCHA, implementado pela Udemy/Google

# Tecnologias utilizadas
- Selenium (Web Scraping) com o navegador Chrome